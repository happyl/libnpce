# -*-encoding:utf-8-*-

import sys
import time
import requests
import urllib
import json
import random

useraget='Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'

def crawler_weibo_wenzhang(url):
    requests.packages.urllib3.disable_warnings()
    cookies=[]
    headers_1 = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Connection': 'keep-alive',
        'Host': 'weibo.com',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': useraget
    }
    ret=requests.get(url, headers=headers_1, allow_redirects=False, verify=False)
    cookies.append(ret.headers['Set-Cookie'])
    referer=ret.headers['Location']

    url_visitor='https://passport.weibo.com/visitor/genvisitor'
    headers_2 = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Referer': referer,
        'Origin':'https://passport.weibo.com',
        'User-Agent': useraget,
        'Cache-Control':'max-age=0',
        'If-Modified-Since': '0'
    }

    param='{"os":"1","browser":"Chrome58,0,3029,110","fonts":"undefined","screenInfo":"1366*768*24","plugins":"Enables Widevine licenses for playback of HTML audio/video content. (version: 1.4.8.970)::widevinecdmadapter.dll::Widevine Content Decryption Module|::mhjfbmdgcfjbbpaeojofohoefgiehjai::Chrome PDF Viewer|::internal-nacl-plugin::Native Client|Portable Document Format::internal-pdf-viewer::Chrome PDF Viewer"}'
    data='cb=gen_callback&fp='+ urllib.quote(param)
    newret = requests.post(url_visitor, data=data,headers=headers_2, allow_redirects=False, verify=False)
    if(newret.status_code==200):
        content=newret.text
        start=content.find('(')
        stop=content.find(')',start)
        jsontext=content[start+1:stop]
        usefultext=json.loads(jsontext)
        tid=usefultext['data']['tid']

        random.seed(10)
        newurl_tmp='https://passport.weibo.com/visitor/visitor?a=incarnate&t={tid}&w=2&c=095&gc=&cb=cross_domain&from=weibo&_rand={r}'
        newurl=newurl_tmp.format(tid=urllib.quote(tid),r=random.random())

        headers_3 = {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Referer': referer,
            'Cookie':'tid='+tid,
            'User-Agent': useraget,
        }
        nextret = requests.get(newurl,  headers=headers_3, allow_redirects=False,verify=False)
        if(200==newret.status_code):
            cookies.append(nextret.headers['Set-Cookie'])

            cookiesstr=''
            for oneitem in cookies:
                cookiesstr=cookiesstr+';'+oneitem

            cookiesstr=cookiesstr.strip(';')
            headers_4 = {
                'Accept': '*/*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'Referer': referer,
                'Cookie': cookiesstr,
                'User-Agent': useraget,
            }
            res = requests.get(url, headers=headers_4, allow_redirects=False, verify=False)
            return res.text

    return None

'''
if __name__ == '__main__':
	reload(sys)
	sys.setdefaultencoding('utf-8')
	url='https://weibo.com/ttarticle/p/show?id=2309404169391527855345&mod=zwenzhang'
	html=crawler_weibo_wenzhang(url)
	if(html):
        	print html
      	else:
         	print 'Download error!'
'''
